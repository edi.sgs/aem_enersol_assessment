SELECT [t3].[uniqueName] AS [PlatformName], [t2].[id] AS [Id], [t2].[platformId] AS [PlatformId], [t2].[uniqueName] AS [UniqueName], [t2].[latitude] AS [Latitude], [t2].[longitude] AS [Longitude], [t2].[createdAt] AS [CreatedAt], [t2].[updatedAt] AS [UpdatedAt]
FROM (
    SELECT MAX([t0].[updatedAt]) AS [value], [t0].[platformId]
    FROM [Wells] AS [t0]
    GROUP BY [t0].[platformId]
    ) AS [t1]
CROSS JOIN [Wells] AS [t2]
INNER JOIN [Platforms] AS [t3] ON [t2].[platformId] = [t3].[id]
WHERE ([t2].[updatedAt] = [t1].[value]) AND ([t1].[platformId] = [t2].[platformId])
ORDER BY PlatformId