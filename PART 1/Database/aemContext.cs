﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assessment.Database
{
    public class aemContext:DbContext
    {
        public aemContext():base()
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source=(LocalDb)\MSSQLLocalDB;Initial Catalog=aem_assesment;Integrated Security=SSPI");
        }

        public DbSet<Platform> Platforms { get; set; }
        public DbSet<Well> Wells { get; set; }
    }
}
