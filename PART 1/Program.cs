﻿using Assessment.Database;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace Assessment
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            await Log("App started");

            //init db context
            var db = new aemContext();

            bool run = true;

            var client = new RestClient(new RestClientOptions("http://test-demo.aemenersol.com/"));

            var loginReq = new RestRequest("/api/Account/Login", Method.Post);
            loginReq.AddJsonBody(@"
            {
              'username': 'user@aemenersol.com',
              'password': 'Test@123'
            }
            ");

            try
            {
                var bearer = await client.ExecuteAsync(loginReq);
                if (bearer != null)
                {
                    client.AddDefaultHeader("Authorization", "Bearer " + bearer.Content.Replace("\"", ""));
                    await Log("login successfull");
                }
            }
            catch (Exception ex)
            {
                await Log("ERROR|" + ex.Message);
                run = false;
            }


            if (run)
            {
                try
                {
                    var dataReq = new RestRequest("api/PlatformWell/GetPlatformWellActual", Method.Get);
                    //var dataReq = new RestRequest("api/PlatformWell/GetPlatformWellDummy", Method.Get);
                    var res = await client.ExecuteAsync(dataReq);

                    if (res != null && res.Content != null)
                    {
                        //this usefull where ignoring members than missing in GetPlatformWellDummy is called
                        var jsonSetting = new JsonSerializerSettings
                        {
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };

                        var data = JsonConvert.DeserializeObject<List<Platform>>(res.Content, jsonSetting);
                        if (data != null && res.Content != null)
                        {
                            foreach (var platform in data)
                            {
                                var p = db.Platforms.AsNoTracking().FirstOrDefault( p => p.id == platform.id);
                                if (p != null)
                                {
                                    p = Update(platform, db.Platforms, db);
                                }
                                else
                                {
                                    db.Platforms.Add(platform);
                                }
                                db.SaveChanges();

                                foreach (var well in platform.well)
                                {
                                    if (db.Wells.Any(w => w.id == well.id && w.platformId == platform.id))
                                    {
                                        var w = Update(well, db.Wells, db);
                                    }
                                    else
                                    {
                                        db.Wells.Add(well);
                                        
                                    }
                                    db.SaveChanges();
                                }

                                
                            }
                            await Log("Syncronization is DONE");
                        }
                        else
                        {
                            await Log("No data");
                        }
                    }
                }
                catch (Exception ex)
                {
                    await Log("ERROR|" + ex);
                    run = false;
                }
            }
            else
            {
                await Log("Cannot fecth data from API");
            }
        }

        //this method for shortcut only
        static async Task Log(string message)
        {
            var msg = $"{DateTime.Now}|{message}";
            await File.AppendAllTextAsync("app.log", msg + "\r\n");
            Console.WriteLine(msg);
        }

        //this update method used for update entity while ignoring null member when GetPlatformWellDummy is called
        public static T Update<T>(T entity, DbSet<T> dbSet, DbContext dbContext) where T : class
        {
            dbSet.Attach(entity);
            dbContext.Entry(entity).State = EntityState.Modified;

            var entry = dbContext.Entry(entity);

            Type type = typeof(T);
            PropertyInfo[] properties = type.GetProperties();
            foreach (PropertyInfo property in properties)
            {
                if (property.GetValue(entity, null) == null)
                {
                    entry.Property(property.Name).IsModified = false;
                }
            }

            dbContext.SaveChanges();
            return entity;
        }
    }
}